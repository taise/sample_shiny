library('shiny')

shinyUI(fluidPage(
  titlePanel("Hello Shiny!"),
    mainPanel(
      plotOutput("distPlot"),
      plotOutput("gaDevicePlot")
    )
))
