
```sql
create database ga;

create table `ga`.`sessions`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `mobile` int(11) NOT NULL DEFAULT '0',
  `no_referrer` int(11) NOT NULL DEFAULT '0',
  `single_session_user` int(11) NOT NULL DEFAULT '0',
  `tablet_pc` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

load data local infile './ga_dataset.csv' into table `ga`.`sessions` fields terminated by ',';
```

```R
install.packages('shiny')
install.packages('DBI')
install.packages('RMySQL')

library('shiny')
runApp('./')
```
